---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
price: {{ .Price }}
---
